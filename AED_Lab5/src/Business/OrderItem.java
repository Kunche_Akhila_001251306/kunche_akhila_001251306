/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author akhila
 */
public class OrderItem {
  private int quantity;
private double salesPrices;
private Product product;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getSalesPrices() {
        return salesPrices;
    }

    public void setSalesPrices(double salesPrices) {
        this.salesPrices = salesPrices;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

 @Override
    public String toString() {
        return product.getProdName(); //To change body of generated methods, choose Tools | Templates.
    }
}