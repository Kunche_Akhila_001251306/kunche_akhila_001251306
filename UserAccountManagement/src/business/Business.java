
package business;

public class Business {

    public static Business business;
    private UserAccountDirectory userAccoutDirectory;
    private EmployeeDirectory employeeDirectory;
    

    private Business() {
        userAccoutDirectory = new UserAccountDirectory();
        employeeDirectory = new EmployeeDirectory();
    }

    public static Business getInstance() {
        if (business == null) {
            business = new Business();
        }
        return business;
    }

    public UserAccountDirectory getUserAccoutDirectory() {
        return userAccoutDirectory;
    }

    public void setUserAccoutDirectory(UserAccountDirectory userAccoutDirectory) {
        this.userAccoutDirectory = userAccoutDirectory;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

}
