
package business;

public class Employee extends Person {

    private int employeeId;
    private String organisation;
    private static int count = 2000;

    public Employee() {
        employeeId = count++;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }
    
    @Override
    public String toString(){
        return this.getFirstName();
    }
}
