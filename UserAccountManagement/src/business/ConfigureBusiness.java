
package business;


public class ConfigureBusiness {

    public static Business initializeBusiness() {
        Business business = Business.getInstance();
        
        //Create admin as employee
        Employee e=business.getEmployeeDirectory().addEmployee();
        e.setFirstName("Admin");
        e.setLastName("");
        e.setOrganisation("Neu");
        
        //Create user account for admin
        UserAccount ua=business.getUserAccoutDirectory().addUserAccount();
        ua.setUserName("admin");
        ua.setPassword("admin");
        ua.setPerson(e);
        ua.setRole(UserAccount.ADMIN_ROLE);
        ua.setIsActive(true);
        
        return business;
    }

}
