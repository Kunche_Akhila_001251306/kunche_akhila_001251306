
package business;


public class Person {

    private String firstName;
    private String lastName;
    private static int count = 1000;
    private int personId;

    public Person() {
        personId = count++;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    @Override
    public String toString() {
        return this.getFirstName() +" "+ this.getLastName();
    }

}
