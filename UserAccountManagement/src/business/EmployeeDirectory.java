
package business;

import java.util.ArrayList;


public class EmployeeDirectory {

    private ArrayList<Employee> employeeList;

    public EmployeeDirectory() {
        employeeList = new ArrayList<Employee>();
    }

    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(ArrayList<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public Employee addEmployee() {
        Employee employee = new Employee();
        employeeList.add(employee);
        return employee;
    }

    public void deleteEmployee(Employee employee) {
        employeeList.remove(employee);
    }
}
