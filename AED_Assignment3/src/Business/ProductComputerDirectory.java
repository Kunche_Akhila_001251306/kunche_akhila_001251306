/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author akhila
 */
public class ProductComputerDirectory {
    private ArrayList<ProductComputer> productList;
    private ArrayList<ProductComputer> printerList;
    private ArrayList<ProductComputer> hardwareList;
    private ArrayList<ProductComputer> softwareList;
    

    public ProductComputerDirectory() {
        this.productList = new ArrayList<ProductComputer>();
        this.printerList = new ArrayList<ProductComputer>();
        this.hardwareList = new ArrayList<ProductComputer>();
        this.softwareList = new ArrayList<ProductComputer>();
    }

    public ArrayList<ProductComputer> getHardwareList() {
        return hardwareList;
    }

    public void setHardwareList(ArrayList<ProductComputer> hardwareList) {
        this.hardwareList = hardwareList;
    }

    public ArrayList<ProductComputer> getSoftwareList() {
        return softwareList;
    }

    public void setSoftwareList(ArrayList<ProductComputer> softwareList) {
        this.softwareList = softwareList;
    }

    public ArrayList<ProductComputer> getPrinterList() {
        return printerList;
    }

    public void setPrinterList(ArrayList<ProductComputer> printerList) {
        this.printerList = printerList;
    }

    public ArrayList<ProductComputer> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<ProductComputer> productList) {
        this.productList = productList;
    }

   
    public ProductComputer addProduct() {
        ProductComputer product = new ProductComputer();
        productList.add(product);
        return product;
    }
    public ProductComputer addProduct2() {
        ProductComputer product = new ProductComputer();
        hardwareList.add(product);
        return product;
    }
    public ProductComputer addProduct3() {
        ProductComputer product = new ProductComputer();
        softwareList.add(product);
        return product;
    }
    public ProductComputer addProduct1() {
        ProductComputer product = new ProductComputer();
        printerList.add(product);
        return product;
    }
    
    
    public void deleteProduct(ProductComputer product) {
        productList.remove(product);
    }
    public void deleteProduct2(ProductComputer product) {
        hardwareList.remove(product);
    }
    public void deleteProduct3(ProductComputer product) {
        softwareList.remove(product);
    }
    public void deleteProduct1(ProductComputer product) {
        printerList.remove(product);
    }
    
    public ProductComputer searchProduct1(String modelNumber) {
        for(ProductComputer product : productList) {
            if(product.getModelNumber().equals(modelNumber)) {
                return product;
            }
        }
        return null;
    }
    
      public ProductComputer searchProduct2(String modelNumber) {
        for(ProductComputer product : hardwareList) {
            if(product.getModelNumber().equals(modelNumber)) {
                return product;
            }
        }
        return null;
    }
        public ProductComputer searchProduct3(String modelNumber) {
        for(ProductComputer product : softwareList) {
            if(product.getModelNumber().equals(modelNumber)) {
                return product;
            }
        }
        return null;
    }
    /*public ProductComputer searchProduct1(String ProductName, String modelNumber, String vendorName, String productDesc, Integer basePrice, Integer ceilingPrice, Integer floorPrice, String productFeatures, String benefits)
    {
        for(ProductComputer product : productList) {
            String xyz = txtSearch.getText();
            if()
            if(product.getModelNumber().equals(modelNumber)) {
                return product;
            }
        }
        return null;
    }*/
      
    public ProductComputer searchProduct(String modelNumber) {
        for(ProductComputer product : printerList) {
            if(product.getModelNumber().equals(modelNumber)) {
                return product;
            }
        }
        return null;
    }
      public ProductComputer searchProduct00(String productName) {
        for(ProductComputer product : printerList) {
            if(product.getProductName().equals(productName)) {
                return product;
            }
        }
        return null;
    }
      public ProductComputer searchProduct01(String vendorName) {
        for(ProductComputer product : printerList) {
            if(product.getVendorName().equals(vendorName)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct02(String productDesc) {
        for(ProductComputer product : printerList) {
            if(product.getProductDesc().equals(productDesc)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct03(String productFeatures) {
        for(ProductComputer product : printerList) {
            if(product.getProductFeatures().equals(productFeatures)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct04(String benefits) {
        for(ProductComputer product : printerList) {
            if(product.getBenefits().equals(benefits)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct11(String productName) {
        for(ProductComputer product : productList) {
            if(product.getProductName().equals(productName)) {
                return product;
            }
        }
        return null;
    }
      public ProductComputer searchProduct12(String vendorName) {
        for(ProductComputer product : productList) {
            if(product.getVendorName().equals(vendorName)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct13(String productDesc) {
        for(ProductComputer product : productList) {
            if(product.getProductDesc().equals(productDesc)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct14(String productFeatures) {
        for(ProductComputer product : productList) {
            if(product.getProductFeatures().equals(productFeatures)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct15(String benefits) {
        for(ProductComputer product : productList) {
            if(product.getBenefits().equals(benefits)) {
                return product;
            }
        }
        return null;
    }
     public ProductComputer searchProduct21(String productName) {
        for(ProductComputer product : hardwareList) {
            if(product.getProductName().equals(productName)) {
                return product;
            }
        }
        return null;
    }
      public ProductComputer searchProduct22(String vendorName) {
        for(ProductComputer product : hardwareList) {
            if(product.getVendorName().equals(vendorName)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct23(String productDesc) {
        for(ProductComputer product : hardwareList) {
            if(product.getProductDesc().equals(productDesc)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct24(String productFeatures) {
        for(ProductComputer product : hardwareList) {
            if(product.getProductFeatures().equals(productFeatures)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct25(String benefits) {
        for(ProductComputer product : hardwareList) {
            if(product.getBenefits().equals(benefits)) {
                return product;
            }
        }
        return null;
    }
     public ProductComputer searchProduct31(String productName) {
        for(ProductComputer product : softwareList) {
            if(product.getProductName().equals(productName)) {
                return product;
            }
        }
        return null;
    }
      public ProductComputer searchProduct32(String vendorName) {
        for(ProductComputer product : softwareList) {
            if(product.getVendorName().equals(vendorName)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct33(String productDesc) {
        for(ProductComputer product : softwareList) {
            if(product.getProductDesc().equals(productDesc)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct34(String productFeatures) {
        for(ProductComputer product : softwareList) {
            if(product.getProductFeatures().equals(productFeatures)) {
                return product;
            }
        }
        return null;
    }
    public ProductComputer searchProduct35(String benefits) {
        for(ProductComputer product : softwareList) {
            if(product.getBenefits().equals(benefits)) {
                return product;
            }
        }
        return null;
    }
    /*public ProductComputer searchProduct05(Float basePrice) {
        for(ProductComputer product : printerList) {
            if(String.valueOf(product.getBasePrice()).equals(basePrice)) {
                return product;
            }
        }
        return null;
    }*/
    
    
}
