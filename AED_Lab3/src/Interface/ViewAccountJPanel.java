/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Account;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author akhila
 */
public class ViewAccountJPanel extends javax.swing.JPanel {
     private JPanel userProcessContainer;
    private Account account;

    /**
     * Creates new form ViewAccountJPanel
     */
   

    ViewAccountJPanel(JPanel userProcessContainer, Account account) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.account = account;
        populateAccountDetails();
        btnSave.setEnabled(false);
        btnUpdate.setEnabled(true);

    }

    private void populateAccountDetails() {
        txtRoutingNum.setText(account.getRountingNumber());
        txtAccountNum.setText(account.getAccountNumber());
        txtBankName.setText(account.getBankName());
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtRoutingNum = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtAccountNum = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtBankName = new javax.swing.JTextField();
        btnBack = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel1.setText("VIEW ACCOUNT");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 70, -1, -1));

        jLabel2.setText("Routing Number");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 170, -1, -1));

        txtRoutingNum.setEnabled(false);
        txtRoutingNum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRoutingNumActionPerformed(evt);
            }
        });
        add(txtRoutingNum, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 160, 234, 36));

        jLabel3.setText("Account Number");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 230, -1, -1));

        txtAccountNum.setEnabled(false);
        txtAccountNum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAccountNumActionPerformed(evt);
            }
        });
        add(txtAccountNum, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 220, 234, 36));

        jLabel4.setText("Bank Name");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 280, -1, -1));

        txtBankName.setEnabled(false);
        txtBankName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBankNameActionPerformed(evt);
            }
        });
        add(txtBankName, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 270, 234, 36));

        btnBack.setText("<<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 535, -1, -1));

        btnSave.setText("Save");
        btnSave.setEnabled(false);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 330, -1, -1));

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        add(btnUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 330, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void txtRoutingNumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRoutingNumActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRoutingNumActionPerformed

    private void txtAccountNumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAccountNumActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAccountNumActionPerformed

    private void txtBankNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBankNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBankNameActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length-1];
        ManageAccountJPanel manageAccountJPanel = (ManageAccountJPanel) component;
        manageAccountJPanel.refreshTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        txtRoutingNum.setEnabled(true);
        txtAccountNum.setEnabled(true);
        txtBankName.setEnabled(true);
        btnSave.setEnabled(true);
        btnUpdate.setEnabled(false);
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        account.setRountingNumber(txtRoutingNum.getText());
        account.setAccountNumber(txtAccountNum.getText());
        account.setBankName(txtBankName.getText());
        
        btnSave.setEnabled(false);
        btnUpdate.setEnabled(true);
        
        JOptionPane.showMessageDialog(null, "Account Updated Sucessfully.");
    }//GEN-LAST:event_btnSaveActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField txtAccountNum;
    private javax.swing.JTextField txtBankName;
    private javax.swing.JTextField txtRoutingNum;
    // End of variables declaration//GEN-END:variables
}
