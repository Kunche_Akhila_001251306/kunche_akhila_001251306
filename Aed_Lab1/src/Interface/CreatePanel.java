/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;
import Business.Product;
import javax.swing.JOptionPane;
/**
 *
 * @author akhila
 */
public class CreatePanel extends javax.swing.JPanel {
    
    private Product product;

    /**
     * Creates new form createJPanel
     */
    public CreatePanel(Product product) {
        initComponents();
        this.product = product;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        createLabel = new javax.swing.JLabel();
        nameLabel = new javax.swing.JLabel();
        priceLabel = new javax.swing.JLabel();
        availNumberLabel = new javax.swing.JLabel();
        descLabel = new javax.swing.JLabel();
        nameTextField = new javax.swing.JTextField();
        priceTextField = new javax.swing.JTextField();
        availNumberTextField = new javax.swing.JTextField();
        descTextField = new javax.swing.JTextField();
        createButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 204, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        createLabel.setFont(new java.awt.Font("Monospaced", 1, 24)); // NOI18N
        createLabel.setText("CREATE A PRODUCT");
        add(createLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(81, 39, 235, 26));

        nameLabel.setBackground(new java.awt.Color(255, 255, 255));
        nameLabel.setText("Name:");
        add(nameLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(46, 88, -1, -1));

        priceLabel.setText("Price:");
        add(priceLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(46, 122, -1, -1));

        availNumberLabel.setText("Availability Number:");
        add(availNumberLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(46, 156, -1, -1));

        descLabel.setText("Description:");
        add(descLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(46, 193, -1, -1));

        nameTextField.setActionCommand("<Not Set>");
        nameTextField.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        nameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTextFieldActionPerformed(evt);
            }
        });
        add(nameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 83, 122, -1));
        add(priceTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 117, 122, -1));
        add(availNumberTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 151, 122, -1));
        add(descTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 188, 122, -1));

        createButton.setText("Create");
        createButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createButtonActionPerformed(evt);
            }
        });
        add(createButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 232, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void createButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createButtonActionPerformed
        // TODO add your handling code here:
        product.setName(nameTextField.getText());
        product.setPrice(priceTextField.getText());
        product.setAvailNum(availNumberTextField.getText());
        product.setDescription(descTextField.getText());
        
        //if(nameTextField.equals("") ||  &&  priceTextField.equals("") && availNumberTextField.equals("") && descTextField.equals("")) {
        //JOptionPane.showMessageDialog(null, "Please enter all the details");}
        //else{
            JOptionPane.showMessageDialog(null, "Created Product Sucessfully");
            
        //}
        
    }//GEN-LAST:event_createButtonActionPerformed

    private void nameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameTextFieldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel availNumberLabel;
    private javax.swing.JTextField availNumberTextField;
    private javax.swing.JButton createButton;
    private javax.swing.JLabel createLabel;
    private javax.swing.JLabel descLabel;
    private javax.swing.JTextField descTextField;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JLabel priceLabel;
    private javax.swing.JTextField priceTextField;
    // End of variables declaration//GEN-END:variables
}
