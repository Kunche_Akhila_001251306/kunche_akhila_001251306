/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.VitalSignHistory;
import Business.VitalSigns;
import javax.swing.JOptionPane;

/**
 *
 * @author akhila
 */
public class CreateJPanel extends javax.swing.JPanel {

    /**
     * Creates new form createJPanel
     */
    
    private VitalSignHistory vsh;
    
    public CreateJPanel(VitalSignHistory vsh) {
        initComponents();
        this.vsh= vsh;
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        createLabel = new javax.swing.JLabel();
        tempLabel = new javax.swing.JLabel();
        tempTextField = new javax.swing.JTextField();
        bpLabel = new javax.swing.JLabel();
        bpTextField = new javax.swing.JTextField();
        pulseLabel = new javax.swing.JLabel();
        pulseTextField = new javax.swing.JTextField();
        dateLabel = new javax.swing.JLabel();
        dateTextField = new javax.swing.JTextField();
        createButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        createLabel.setFont(new java.awt.Font("Adobe Garamond Pro", 1, 14)); // NOI18N
        createLabel.setText("CREATE VITAL SIGN");
        add(createLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 50, 152, 51));

        tempLabel.setText("Temperature");
        add(tempLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 140, -1, -1));
        add(tempTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 130, 149, -1));

        bpLabel.setText("Blood Pressure");
        add(bpLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 180, -1, -1));
        add(bpTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 170, 149, -1));

        pulseLabel.setText("Pulse");
        add(pulseLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 220, -1, -1));
        add(pulseTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 210, 149, -1));

        dateLabel.setText("Date");
        add(dateLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 250, -1, -1));
        add(dateTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 250, 149, -1));

        createButton.setText("Create Vital Sign");
        createButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createButtonActionPerformed(evt);
            }
        });
        add(createButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 320, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void createButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createButtonActionPerformed
        // TODO add your handling code here:
        double temperature = Double.parseDouble(tempTextField.getText());
        double bloodPressure = Double.parseDouble(bpTextField.getText());
        Integer pulse = Integer.parseInt(pulseTextField.getText());
        String date = dateTextField.getText();
        
        VitalSigns v = vsh.addVitals();
        v.setTemperature(temperature);
        v.setBloodPressure(bloodPressure);
        v.setPulse(pulse);
        v.setDate(date);
        
        JOptionPane.showMessageDialog(null, "Vital Signs Created Sucesssfully");
        
        tempTextField.setText("");
        bpTextField.setText("");
        pulseTextField.setText("");
        dateTextField.setText("");
        
    }//GEN-LAST:event_createButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bpLabel;
    private javax.swing.JTextField bpTextField;
    private javax.swing.JButton createButton;
    private javax.swing.JLabel createLabel;
    private javax.swing.JLabel dateLabel;
    private javax.swing.JTextField dateTextField;
    private javax.swing.JLabel pulseLabel;
    private javax.swing.JTextField pulseTextField;
    private javax.swing.JLabel tempLabel;
    private javax.swing.JTextField tempTextField;
    // End of variables declaration//GEN-END:variables
}
