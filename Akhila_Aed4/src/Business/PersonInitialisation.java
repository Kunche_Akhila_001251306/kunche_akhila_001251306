/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import Business.Student;
import Business.StudentList;
import Business.WorkHistory;
import Business.Certifications;

/**
 *
 * @author Gayathri
 */
public class PersonInitialisation {
    
public static StudentList initializeStudentList() {
        Transcript transcript = new Transcript();
        WorkHistory workHistory = new WorkHistory();
        StudentList studentList = new StudentList();
        Certifications certifications = new Certifications();
        Student student1 = studentList.addStudent();
        student1.setFirstName("Akhila");
        student1.setLastName("Kunche");
        student1.setId("1251306");
        transcript.transcript1();
        student1.setTranscript(transcript);
        workHistory.experience1();
        certifications.Certifications1();
        
        
        Student student2 = studentList.addStudent();
        student2.setFirstName("Lahari");
        student2.setLastName("Palle");
        student2.setId("1251305");
        transcript.transcript2();
        student2.setTranscript(transcript);
        workHistory.experience2();
        certifications.Certifications2();

        
        Student student3 = studentList.addStudent();
        student3.setFirstName("Gayathri");
        student3.setLastName("Maganti");
        student3.setId("1251304");
        transcript.transcript3();
        student3.setTranscript(transcript);
        workHistory.experience3();
        certifications.Certifications3();

        return studentList;
       
        
  }
}