/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author akhila
 */
public class StudentList {
    
    List<Student> studentList;
    private Transcript transcript;
    private WorkHistory workHistory;
    private Certifications certifications;

    public WorkHistory getWorkHistory() {
        return workHistory;
    }

    public void setWorkHistory(WorkHistory workHistory) {
        this.workHistory = workHistory;
    }

    public Certifications getCertifications() {
        return certifications;
    }

    public void setCertifications(Certifications certifications) {
        this.certifications = certifications;
    }
    
    public Transcript getTranscript() {
        return transcript;
    }

    public void setTranscript(Transcript transcript) {
        this.transcript = transcript;
    }


    public StudentList() {
        studentList = new ArrayList<>();
        transcript = new Transcript();
        workHistory = new WorkHistory();
        certifications = new Certifications();
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public Student addStudent() {
        Student student = new Student();
        studentList.add(student);
        return student;
    }
        
  
}
