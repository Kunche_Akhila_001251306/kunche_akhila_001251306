/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Person;

import java.util.ArrayList;

/**
 *
 * @author akhila
 */
public class ResumeHistory {
    
     
    private ArrayList<Resume> resumeHistory;
    
    public ResumeHistory() {
        resumeHistory = new ArrayList<Resume>();
    }

    public ArrayList<Resume> getResumeHistory() {
        return resumeHistory;
    }

    public void setResumeHistory(ArrayList<Resume> resumeHistory) {
        this.resumeHistory = resumeHistory;
    }
    
    public Resume addDetails() {
        Resume rs = new Resume();
        resumeHistory.add(rs);
        return rs;
    }
    
    public void deleteDetails(Resume r) {
        resumeHistory.remove(r); 
    }
    
}
