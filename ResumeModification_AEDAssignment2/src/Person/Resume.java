/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Person;

import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.Icon;

/**
 *
 * @author akhila
 */
public class Resume {
    
    private String firstName;
    private String lastName;
    private String streetName,suiteName,city,state,country;
    private String zipCode;
    private String emailId;
    private String phone;
    private String affiliation;
    private String careerObj;
    private String highlights;
    private String additionalInfo;
    private String nameOfInst1,degreeName1,endDate1,grade1,nameOfInst2,degreeName2,endDate2,grade2;
    private Icon picture;
    private String YearsOfExp;
    private String radioGraduation;
    private String radioGender;
    private String radioCitizenship;
    private String checkC;
    private String checkCpp;
    private String checkJava;
    private String checkDbms;
    private String checkHtml;

    public String getYearsOfExp() {
        return YearsOfExp;
    }

    public void setYearsOfExp(String YearsOfExp) {
        this.YearsOfExp = YearsOfExp;
    }

    public String getRadioGraduation() {
        return radioGraduation;
    }

    public void setRadioGraduation(String radioGraduation) {
        this.radioGraduation = radioGraduation;
    }

    public String getRadioGender() {
        return radioGender;
    }

    public void setRadioGender(String radioGender) {
        this.radioGender = radioGender;
    }

    public String getRadioCitizenship() {
        return radioCitizenship;
    }

    public void setRadioCitizenship(String radioCitizenship) {
        this.radioCitizenship = radioCitizenship;
    }

    public String getCheckC() {
        return checkC;
    }

    public void setCheckC(String checkC) {
        this.checkC = checkC;
    }

    public String getCheckCpp() {
        return checkCpp;
    }

    public void setCheckCpp(String checkCpp) {
        this.checkCpp = checkCpp;
    }

    public String getCheckJava() {
        return checkJava;
    }

    public void setCheckJava(String checkJava) {
        this.checkJava = checkJava;
    }

    public String getCheckDbms() {
        return checkDbms;
    }

    public void setCheckDbms(String checkDbms) {
        this.checkDbms = checkDbms;
    }

    public String getCheckHtml() {
        return checkHtml;
    }

    public void setCheckHtml(String checkHtml) {
        this.checkHtml = checkHtml;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getSuiteName() {
        return suiteName;
    }

    public void setSuiteName(String suiteName) {
        this.suiteName = suiteName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation1) {
        this.affiliation = affiliation1;
    }

    public String getCareerObj() {
        return careerObj;
    }

    public void setCareerObj(String careerObj) {
        this.careerObj = careerObj;
    }

    public String getHighlights() {
        return highlights;
    }

    public void setHighlights(String highlights) {
        this.highlights = highlights;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getNameOfInst1() {
        return nameOfInst1;
    }

    public void setNameOfInst1(String nameOfInst1) {
        this.nameOfInst1 = nameOfInst1;
    }

    public String getDegreeName1() {
        return degreeName1;
    }

    public void setDegreeName1(String degreeName1) {
        this.degreeName1 = degreeName1;
    }

    public String getEndDate1() {
        return endDate1;
    }

    public void setEndDate1(String endDate1) {
        this.endDate1 = endDate1;
    }

    public String getGrade1() {
        return grade1;
    }

    public void setGrade1(String grade1) {
        this.grade1 = grade1;
    }

    public String getNameOfInst2() {
        return nameOfInst2;
    }

    public void setNameOfInst2(String nameOfInst2) {
        this.nameOfInst2 = nameOfInst2;
    }

    public String getDegreeName2() {
        return degreeName2;
    }

    public void setDegreeName2(String degreeName2) {
        this.degreeName2 = degreeName2;
    }

    public String getEndDate2() {
        return endDate2;
    }

    public void setEndDate2(String endDate2) {
        this.endDate2 = endDate2;
    }

    public String getGrade2() {
        return grade2;
    }

    public void setGrade2(String grade2) {
        this.grade2 = grade2;
    }

    public Icon getPicture() {
        return picture;
    }

    public void setPicture(Icon picture) {
        this.picture = picture;
    }

    @Override 
    public String toString() {
        return this.firstName;
}

    public void setPicture(String picture) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}